const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");

const Empresa = require('../modelos/empresa');

router.get('/', (req,res,next)=>{
    Empresa.find()
    .exec()
    .then(resultado=>{
        res.status(200).json({
            estado:200,
            empresas:resultado
        });
    })
    .catch(err=>{
        res.status(500).json({
            estado:500,
            error:err
        });
    });
});

router.post('/',(req,res,next) =>{
    let body = req.body;
    const empresa = new Empresa({
        _id: new mongoose.Types.ObjectId(),
        id_empresa: body.id_usuario,
        rut:body.rut,
        nombre:body.nombres,
        fantasia:body.fantasia,
        logo:body.logo, 
        id_pais:body.id_pais, 
        id_region:body.id_region,
        id_ciudad:body.id_ciudad,
        id_comuna:body.id_comuna,
        direccion:body.direccion, 
        diamantes:body.diamantes,
        estrellas:body.estrellas,
        visualizacion:body.visualizacion,
        creacion:body.creacion,
    });
    empresa
        .save()
        .then(resultado =>{
            res.status(201).json({
                estado:201,
                mensaje:'Empresa creada',
                empresa:resultado
            });
    })
    .catch(err=>{
        res.status(500).json({
            estado:500,
            error:err
        });
    });
});

module.exports = router;