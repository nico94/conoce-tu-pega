const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
var bcrypt = require('bcryptjs');

const Usuario = require('../modelos/usuario');

router.get('/', (req,res,next)=>{
    Usuario.find()
    .exec()
    .then(usuario=>{
        res.status(200).json({
            estado:200,
            usuario:usuario
        });
    })
    .catch(err=>{
        res.status(500).json({
            estado:500,
            error:err
        });
    });
});

router.post('/',(req,res,next) =>{
    let body = req.body;
    var hashedPassword = bcrypt.hashSync(body.password, 8);
    const usuario = new Usuario({
        _id: new mongoose.Types.ObjectId(),
        id_usuario: body.id_usuario,
        rut:body.rut,
        nombres:body.nombres,
        apellidos:body.apellidos,
        sexo:body.sexo, 
        nacimiento:body.nacimiento,
        correo:body.correo, 
        telefono:body.telefono,
        id_pais:body.id_pais, 
        id_region:body.id_region,
        id_ciudad:body.id_ciudad,
        id_comuna:body.id_comuna,
        direccion:body.direccion, 
        usuario:body.usuario, 
        password:hashedPassword,
        creacion:body.creacion,
        diamantes:body.diamantes
    });
    usuario
        .save()
        .then(resultado =>{
            res.status(201).json({
                estado:201,
                mensaje:'Usuario creado',
                usuario:resultado
            });
    })
    .catch(err=>{
        res.status(500).json({
            estado:500,
            error:err
        });
    });
});

module.exports = router;