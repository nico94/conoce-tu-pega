var mongoose = require('mongoose');

const EmpresaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id_empresa:Number,
    rut:String,
    nombre:String,
    fantasia:String,
    logo:String,
    id_pais:Number,
    id_region:Number,
    id_ciudad:Number,
    id_comuna:Number,
    direccion:String,
    diamantes:Number,
    estrellas:Number,
    visualizacion:Number,
    creacion:String
},
    { versionKey: false }
);

module.exports = mongoose.model('empresa',EmpresaSchema);