export const environment = {
  production: false,
  url:'https://conocetupega.herokuapp.com',
  api:{
    usuario: 'https://conocetupega.herokuapp.com/usuario',
    empresa: 'https://conocetupega.herokuapp.com/empresa'
  },
  firebase: {
    apiKey: "AIzaSyAih4_QczFM3W6D7BSBj5K14DhbQmrQ8YE",
    authDomain: "conoce-tu-pega.firebaseapp.com",
    databaseURL: "https://conoce-tu-pega.firebaseio.com",
    projectId: "conoce-tu-pega",
    storageBucket: "conoce-tu-pega.appspot.com",
    messagingSenderId: "977906770864"
  }
};
  
