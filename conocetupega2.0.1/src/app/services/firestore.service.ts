import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from "angularfire2/firestore";
import { Observable } from "rxjs/Rx";
import { IEmpresa } from '../models/empresa';


@Injectable()
export class FirestoreService {
  
  colecion: AngularFirestoreCollection<IEmpresa>;
  empresa: Observable<IEmpresa>;

  constructor(private afs: AngularFirestore) { }

  // getEmpresas(){
  //   this.empresas = this.colecion.snapshotChanges()
  //   .map( changes =>{
  //     return changes.map(action =>{
  //       const data = action.payload.doc.data() as IEmpresa;
  //       data._id = action.payload.doc.id;
  //       return data;
  //     });
  //   });
  //   return this.empresas;
  // }
  addEmpresa(empresa: IEmpresa){
    this.colecion.add(empresa);
  }
  getEmpresas(){
    return this.afs.collection('empresas', ref => ref.orderBy('diamantes', 'desc')).snapshotChanges();
  }
}
