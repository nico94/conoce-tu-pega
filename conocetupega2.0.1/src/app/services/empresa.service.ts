import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';
import { environment } from '../../config/API';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';

@Injectable()
export class EmpresaService {

  constructor( private http:HttpClient ) { }

  getEmpresas(): Observable<any>{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http
    .get<any>(environment.api.empresa, { headers: headers })
    .retry(3)
    .catch((err:any)=>{
      return Observable.throw(err);
    });
   }

}
