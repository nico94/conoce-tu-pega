import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

//Rutas
import { APP_ROUTES } from './app.routes';

//Plugins
import { HttpClientModule } from '@angular/common/http';

//Servicios
import { EmpresaService } from './services/empresa.service';

//Modulos
import { PagesModule } from './pages/pages.module';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    APP_ROUTES,
    HttpClientModule,
    PagesModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    EmpresaService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
