import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../../services/empresa.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styles: []
})
export class EmpresaComponent implements OnInit {

  comentariosArray:any[] = [];
  mostrarComentResp:number = 0;

  constructor(private empresaServicio: EmpresaService) {
    this.comentariosArray = [
      {
        idUser: 1,
        nomUser: "Jared",
        comentInfo: 2,
        imgUser: "https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img (67).jpg",
        desComentario: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem laboriosam modi tenetur excepturi consequatur doloribus minus veniam quidem? Id cumque, excepturi itaque consectetur quia praesentium? Odio corrupti amet obcaecati?",
        resp: [
          {
            respId: 1,
            respUser: "Jonh",
            respDesc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem laboriosam modi tenetur excepturi"
          },
          {
            respId: 1,
            respUser: "Mark Zukenberg",
            respDesc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem laboriosam modi tenetur excepturi"
          }
        ]
      },
      { 
        idUser: 2,
        nomUser: "Gabin Belson",
        comentInfo: 1,
        imgUser: "https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img (67).jpg",
        desComentario: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem laboriosam modi tenetur excepturi consequatur doloribus.",
        resp: [
          {
            respId: 2,
            respUser: "Jonh",
            respDesc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem laboriosam modi tenetur excepturi"
          }
        ]
      },
      { 
        idUser: 3,
        nomUser: "Daresh",
        comentInfo: 3,
        imgUser: "https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img (67).jpg",
        desComentario: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem.",
        resp: [
          {
            respId: 1,
            respUser: "Jonh",
            respDesc: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquam rem laboriosam modi tenetur excepturi"
          }
        ]
      }
    ];
   }

  ngOnInit() {
  }

  mostrarComentario( idUser:number ){
    this.mostrarComentResp = this.mostrarComentResp!=idUser ? idUser : 0;   
  }

}
