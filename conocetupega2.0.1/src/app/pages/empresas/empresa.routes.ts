import { Routes, RouterModule } from "@angular/router";

import { EmpresasComponent } from './empresas.component';
import { EmpresaComponent } from './empresa/empresa.component';

const adminRoutes: Routes = [
    {
        path: '',
        component: EmpresasComponent,
        children:[            
            { path: 'empresas/empresa', component: EmpresaComponent }
        ]
    }    
];


export const EMPRESA_ROUTES = RouterModule.forChild( adminRoutes );