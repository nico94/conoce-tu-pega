import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

//Rutas
import { EMPRESA_ROUTES } from './empresa.routes';


import { SharedModule } from './../../shared/shared.module';
import { CommonModule } from "@angular/common";

import { EmpresasComponent } from "./../empresas/empresas.component";
import { EmpresaComponent } from './empresa/empresa.component';


@NgModule({
    declarations: [    
        EmpresasComponent, EmpresaComponent
    ],
    schemas: [ NO_ERRORS_SCHEMA ],
    exports: [    
        EmpresasComponent
    ],
    imports:[    
        MDBBootstrapModule.forRoot(),    
        CommonModule,
        SharedModule,
        EMPRESA_ROUTES
    ]
})
export class EmpresasModule { }