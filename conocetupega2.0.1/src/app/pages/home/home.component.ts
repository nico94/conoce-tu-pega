import { Component, OnInit } from '@angular/core';

import { FirestoreService } from '../../services/firestore.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  top5:any = [];
  empresas:any = [];
  cargado:boolean = true;

  constructor( private empresaService: FirestoreService ) 
  {
    this.obtenerEmpresas();
  }

  ngOnInit() {
  }


  obtenerEmpresas(){
    this.empresaService.getEmpresas()
      .subscribe(res => {
        this.top5 = res;
        this.empresas = res;
        this.cargado = false;
      },
      error =>{
        console.log('Error en Webservice ' + error);
      });
  }

}
