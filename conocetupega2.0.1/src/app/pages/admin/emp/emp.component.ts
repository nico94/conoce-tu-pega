import { Component, OnInit } from '@angular/core';

import { FirestoreService } from '../../../services/firestore.service';
import { IEmpresa } from './../../../models/empresa';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp',
  templateUrl: './emp.component.html',
  styles: []
})
export class EmpComponent implements OnInit {

  empresas:any = [];

  empresa : IEmpresa = {
    _id: '',
    ciudad: '',
    comuna: '',
    diamantes: 0,
    direccion: '',
    estrellas: 0,
    fantasia: '',
    logo: '',
    nombre: '',
    pais: '',
    region: '',
    visitas: 0
  }

  constructor( private empresaService: FirestoreService, private router: Router ) { 
    this.obtenerEmpresas();
  }

  ngOnInit() {
  }

  obtenerEmpresas(){
    this.empresaService.getEmpresas()
      .subscribe(res => {
        this.empresas = res;
      },
      error =>{
        console.log('Error en Webservice ' + error);
      });
  }

  onSubmit( {value} : {value: IEmpresa} ){
    value.diamantes = 1;
    value.estrellas = 1;
    value.visitas = 1;
    this.empresaService.addEmpresa(value);
    // this.router.navigate(['/admin']);
  }

}
