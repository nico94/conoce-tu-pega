import { AppComponent } from './../../app.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

//Rutas
import { ADMIN_ROUTES } from "./admin.routes";

import { SharedModule } from './../../shared/shared.module';
import { CommonModule } from "@angular/common";

import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin.component';
import { EmpComponent } from './emp/emp.component';

//Formulario
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [    
        AdminComponent,
        UserComponent,
        EmpComponent
    ],
    schemas: [ NO_ERRORS_SCHEMA ],
    exports: [    
        AdminComponent,
        UserComponent,
        EmpComponent
    ],
    imports:[    
        MDBBootstrapModule.forRoot(),    
        CommonModule,
        SharedModule,
        ADMIN_ROUTES,
        FormsModule
    ],
    bootstrap: [AppComponent]
})
export class AdminModule { }