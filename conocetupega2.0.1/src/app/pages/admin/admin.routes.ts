import { Routes, RouterModule } from "@angular/router";

import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin.component';

const adminRoutes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children:[            
            { path: 'admin/user', component: UserComponent }
        ]
    }    
];


export const ADMIN_ROUTES = RouterModule.forChild( adminRoutes );