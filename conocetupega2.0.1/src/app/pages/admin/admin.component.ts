import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styles: []
})
export class AdminComponent implements OnInit {
  
  adminuser:boolean = false;
  adminempresa:boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showPage(page:number){    
    switch (page) {
      case 1:
        this.adminuser = true;
        this.adminempresa = false;
        break;
      case 2:
        this.adminempresa = true;
        this.adminuser = false;
        break;
    }
  }

}
