import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { AdminComponent } from "./admin/admin.component";
import { EmpresasComponent } from "./empresas/empresas.component";
import { PerfilComponent } from "./perfil/perfil.component";
import { UserComponent } from "./admin/user/user.component";

const pagesRoutes: Routes = [  
    { path: 'inicio', component: HomeComponent },
    { path: 'empresas', component: EmpresasComponent },
    { path: 'perfil', component: PerfilComponent },
    { 
        path: 'admin', 
        component: AdminComponent ,
        children: [
            { path: 'user', component: UserComponent }
        ]
    },
    { path: '', redirectTo: '/inicio', pathMatch: 'full' },
    { path: '**', redirectTo: '/inicio', pathMatch: 'full' }         
];


export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );