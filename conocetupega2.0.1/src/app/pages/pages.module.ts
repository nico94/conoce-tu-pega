import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

//Plugins
import { HttpClientModule } from '@angular/common/http';

//Rutas
import { PAGES_ROUTES } from "./pages.routes";

import { SharedModule } from '../shared/shared.module';
import { CommonModule } from "@angular/common";

import { PagesComponent } from "./pages.component";
import { HomeComponent } from "./home/home.component";
import { PerfilComponent } from "./perfil/perfil.component";
import { AdminModule } from './admin/admin.module';
import { EmpresasModule } from './empresas/empresas.module';

//Firestore
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from "angularfire2/firestore";
import { environment } from "../../config/API";

//Servicios
import { FirestoreService } from '../services/firestore.service';

@NgModule({
    declarations: [    
        PagesComponent,
        HomeComponent,
        PerfilComponent
    ],
    schemas: [ NO_ERRORS_SCHEMA ],
    exports: [   
        PagesComponent,
        HomeComponent,
        PerfilComponent
    ],
    imports:[    
        MDBBootstrapModule.forRoot(),    
        HttpClientModule,
        CommonModule,
        SharedModule,
        PAGES_ROUTES,
        AdminModule,
        EmpresasModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule
    ],
    providers:[
        FirestoreService
    ]
})
export class PagesModule { }