import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
    declarations: [
        HeaderComponent,
        SidebarComponent
    ],
    exports: [
        HeaderComponent,
        SidebarComponent
    ],
    schemas: [ NO_ERRORS_SCHEMA ],
    imports: [        
        MDBBootstrapModule.forRoot() 
    ]
})
export class SharedModule {}
