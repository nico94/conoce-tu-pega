const EmpresaControlador = require('./controladores/empresa');

exports = module.exports = function (io) {
    io.on('connection', (socket) => {
        console.log('Cliente conectado a Socket');

        socket.on('obtener empresas',(data)=>{
            let res = [{ 
                "id":1,
                "des":"Hola"
            }];
            io.emit('enviado',JSON.stringify(res));
        });
       
        socket.on('disconnect', () => {
            console.log('Cliente desconectado a Socket');
        });
    });
};
