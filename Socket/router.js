const UsuarioControlador = require('./controladores/usuario');
const EmpresaControlador = require('./controladores/empresa');

const express = require('express');

module.exports = function(app){
    //Inicializar las rutas
    const rutas = express.Router(),
        usuarioRuta = express.Router(),
        empresaRuta = express.Router();

    //Rutas para usuario
    rutas.use('/usuario', usuarioRuta);
    usuarioRuta.get('/',UsuarioControlador.obtenerUsuarios);
    
    //Rutas para empresa
    rutas.use('/empresa',empresaRuta);
    empresaRuta.get('/',EmpresaControlador.obtenerEmpresas);
    
    //Ruta principal
    app.use('/api', rutas);
};