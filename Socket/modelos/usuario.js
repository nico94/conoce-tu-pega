const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsuarioSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id_usuario: Number,
    rut: String,
    nombres: String,
    apellidos: String,
    sexo: Number,
    nacimiento: String,
    correo: String,
    telefono: String,
    id_pais: Number,
    id_region: Number,
    id_ciudad: Number,
    id_comuna: Number,
    direccion: String,
    usuario: String,
    password: String,
    creacion: String,
    diamantes: Number
},
    { versionKey: false }
);

module.exports = mongoose.model('usuario', UsuarioSchema,'usuarios');
