const Empresa = require('../modelos/empresa');

//Metodo para listar las empresas
exports.obtenerEmpresas = function(req,res,next){
    Empresa.find({})
    .exec(function (err, empresa) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.json(empresa);
        }
    });
}

exports.obtenerEmpresas2 = function (req, res, next) {
    Empresa.find({})
        .exec(function (err, empresa) {

            if (err) {
                return err;
            } else {
                return empresa;
            }
        });
}
