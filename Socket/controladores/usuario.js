const Usuario = require('../modelos/usuario');

exports.obtenerUsuarios = function(req,res,next){
    Usuario.find({})
    .exec(function (err, models) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.json(models);
        }
    });
};