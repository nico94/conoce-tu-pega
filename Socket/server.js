// Importing Node modules and initializing Express
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const logger = require('morgan');
const router = require('./router');
const eventosSocket = require('./eventosSocket');
const mongoose = require('mongoose');
const config = require('./config/config');
const cors = require('cors');

//Conectar a Base de datos
mongoose.connect(config.db.uri, { useNewUrlParser: true });

//Para testing
app.use(cors());

//Iniciar el servidor
let server;
if(process.env.NODE_ENV != config.env){
    server = app.listen(config.puerto);
    console.log('Servidor corriendo en puerto '+config.puerto);
}else{
    server = app.listen(3001);
    console.log('Servidor corriendo en puerto 3001');
}

//Importar el socket
const io = require('socket.io').listen(server);
eventosSocket(io);

// Configuracion de middleware para peticiones Express
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

// Configura las CORS para el cliente
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
});

//Importa rutas
router(app);

module.exports = server;