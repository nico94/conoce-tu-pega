import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { AdminComponent } from './admin/admin.component';
import { AdmUserComponent } from './admin/adm-user/adm-user.component';
import { AdmEmpresaComponent } from './admin/adm-empresa/adm-empresa.component';
import { EmpresaComponent } from './empresas/empresa/empresa.component';
import { CuentaComponent } from './cuenta/cuenta.component';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

//Firestore
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from './../../config/API';

import { EmpresaService } from './../services/empresa.service';
import { AcountService } from '../services/acount.service';

//Formulario
import { FormsModule } from '@angular/forms';
import { PerfilComponent } from './cuenta/perfil/perfil.component';
import { MensajesComponent } from './cuenta/mensajes/mensajes.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
    declarations: [
        HomeComponent,
        EmpresasComponent,
        AdminComponent,
        AdmUserComponent,
        AdmEmpresaComponent,
        EmpresaComponent,
        CuentaComponent,
        PerfilComponent,
        MensajesComponent,
        AboutComponent,
        ContactComponent
    ],
    exports: [
        HomeComponent,
        EmpresasComponent,
        AdminComponent,
        AdmUserComponent,
        AdmEmpresaComponent,
        EmpresaComponent
    ],
    imports: [
        BrowserModule,
        RouterModule,
        CommonModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        FormsModule
    ],
    providers: [
        EmpresaService,
        AcountService
    ]
  })

  export class PagesModule { }