import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { EmpresaService } from './../../../services/empresa.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { IEmpresa } from '../../../models/empresa';

@Component({
  selector: 'app-adm-empresa',
  templateUrl: './adm-empresa.component.html',
  styles: []
})
export class AdmEmpresaComponent implements OnInit {

  empresas:Observable<any[]>;

  empresa : IEmpresa = {
    _id: '',
    ciudad: '',
    comuna: '',
    diamantes: 0,
    direccion: '',
    estrellas: 0,
    fantasia: '',
    logo: '',
    nombre: '',
    pais: '',
    region: '',
    visitas: 0
  }

  constructor(private db: AngularFirestore, private empresaService: EmpresaService) {

  }
  
  ngOnInit() {
    this.obtenerEmpresas();
  }
  
  obtenerEmpresas(){
    this.empresas = this.db.collection('empresas', ref => ref.orderBy('diamantes', 'desc')).valueChanges(); 
  }

  onSubmit( {value} : {value: IEmpresa} ){
    console.log('asdasdasd');
    value.diamantes = 1;
    value.estrellas = 1;
    value.visitas = 1;
    this.empresaService.addEmpresa(value);
  }

}
