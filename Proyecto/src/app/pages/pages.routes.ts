import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { AdmUserComponent } from './admin/adm-user/adm-user.component';
import { AdmEmpresaComponent } from './admin/adm-empresa/adm-empresa.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { EmpresaComponent } from './empresas/empresa/empresa.component';
import { CuentaComponent } from './cuenta/cuenta.component';
import { PerfilComponent } from './cuenta/perfil/perfil.component';
import { MensajesComponent } from './cuenta/mensajes/mensajes.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';



const pageRoutes: Routes = [
    // { path: '**', redirectTo: 'inicio', pathMatch: 'full' },
    { path: '', redirectTo: 'inicio', pathMatch: 'full' },
    { path: 'inicio', component: HomeComponent },
    { path: 'empresa', component: EmpresaComponent },
    { path: 'empresas', component: EmpresasComponent },
    { path: 'about', component: AboutComponent },
    { path: 'contacto', component: ContactComponent },
    {
        path: 'admin',
        component: AdminComponent,
        children: [
            { path: 'user', component: AdmUserComponent },
            { path: 'empresa', component: AdmEmpresaComponent }
        ]
    },
    {
        path: 'cuenta',
        component: CuentaComponent,
        children: [
            { path: 'perfil', component: PerfilComponent },
            { path: 'mensajes', component: MensajesComponent }
        ]
    }
];

export const PAGES_ROUTES = RouterModule.forRoot(pageRoutes);