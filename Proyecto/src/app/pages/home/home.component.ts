import { Component, OnInit } from '@angular/core';

import { EmpresaService } from './../../services/empresa.service';
import { IEmpresa } from './../../models/empresa';

import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  empresas: any;  
  empresastop5: any;
  
  cargando:boolean = true;
  cargando5:boolean = true;

  constructor( db: AngularFirestore, public empresaService: EmpresaService ) { 
  }

  ngOnInit() {
    this.getEmpresasTop5();
    this.getEmpresas();
  }

  getEmpresas(){
    this.empresaService.getEmpresas()
      .subscribe((empresas:any[]) => {
        this.empresas = empresas;
        this.cargando = false;
      });
  }

  getEmpresasTop5(){
    this.empresaService.getEmpresasTop5()
      .subscribe( (empresastop5:any[]) =>{
        this.empresastop5 = empresastop5;
        this.cargando5 = false;
      });
  }

}
