import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styles: []
})
export class MensajesComponent implements OnInit {

  @ViewChild("idTxtMensaje") inputMensaje: ElementRef;
  txtMensaje:string;

  constructor() { }

  ngOnInit() {
  }

  eventHandler(event) {
    // console.log(event);
    // console.log(event.keyCode);
    if(event.keyCode == 13){
      this.enviarMensaje();
    }
  } 

  enviarMensaje(){
    console.log(this.txtMensaje);
    this.txtMensaje = "";
    this.inputMensaje.nativeElement.focus();
  }

}
