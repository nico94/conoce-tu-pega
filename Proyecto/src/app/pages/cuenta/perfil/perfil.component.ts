import { AcountService } from './../../../services/acount.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: []
})
export class PerfilComponent implements OnInit {
  usuario: any = [];
  activa: boolean;

  constructor(
    public _ac: AcountService
  ) {
    this.cargaUsuario();
  }

  ngOnInit() { }

  cargaUsuario() {
    if (this._ac.usuario) {
      this.usuario = this._ac.usuario;
      // if (this.usuario.tipocuenta === "G") {
      //   this.activa = false;
      // } else {
      //   this.activa = true;
      // }
    }
  }
}