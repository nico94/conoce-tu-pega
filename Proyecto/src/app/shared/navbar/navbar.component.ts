import { Component, OnInit } from '@angular/core';
import { AcountService } from './../../services/acount.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {
  buscando: boolean = false;
  logueado: boolean;
  user: any = {};

  constructor(public _ac: AcountService) {
    this._ac.afAuth.authState.subscribe((user) => {
      // console.log("Session: ", user);
      if (!user) {
        this.logueado = false;
      } else {
        this.logueado = true;
      }
    });
  }

  ngOnInit() { }

  buscar() {
    this.buscando = !this.buscando;
  }

  login(proveedor: string) {
    this.logueado = true;
    this._ac.login();
  }

  logout() {
    this.logueado = false;
    this._ac.logout();
  }

  color: any;
  color2: any;

  changeStyle($event) {
    this.color = $event.type == 'mouseover' ? { 'fa-star-o': false, 'fa-star': true } : { 'fa-star': false, 'fa-star-o': true };
  }
  changeStyle2($event) {
    this.changeStyle($event);
    this.color2 = $event.type == 'mouseover' ? { 'fa-star-o': false, 'fa-star': true } : { 'fa-star': false, 'fa-star-o': true };
  }
}