export interface IEmpresa{
    _id?:any;
    ciudad?:string;
    comuna?:string;
    diamantes?:number;
    direccion?:string;
    estrellas?:number;
    fantasia?:string;
    logo?:string;
    nombre?:string;
    pais?:string;
    region?:string;
    visitas?:number;
}