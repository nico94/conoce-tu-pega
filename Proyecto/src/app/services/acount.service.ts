import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';

@Injectable()
export class AcountService {
    public usuario: any = {};

    constructor(public afAuth: AngularFireAuth){
        this.afAuth.authState.subscribe( user =>{
            // console.log("Estado del usuario: ", user);
            if( !user ){
                return;
            }
            
            this.usuario.nombre = user.displayName;
            this.usuario.uid = user.uid;
            this.usuario.foto = user.photoURL;
            this.usuario.email = user.email;
            this.usuario.tipocuenta = "G";
        });
    }

    login() {
        this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    }
    logout() {
        this.afAuth.auth.signOut();
    }
}