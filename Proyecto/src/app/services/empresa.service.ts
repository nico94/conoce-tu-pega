import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from "angularfire2/firestore";
import { Observable } from "rxjs/Rx";
import { IEmpresa } from '../models/empresa';


@Injectable()
export class EmpresaService {
  
  colecion: AngularFirestoreCollection<IEmpresa>;
  doc: AngularFirestoreDocument<IEmpresa>;
  empresa: Observable<IEmpresa>;

  constructor(private afs: AngularFirestore) { 
  }

  addEmpresa(empresa: IEmpresa){
    this.colecion.add(empresa);
  }

  getEmpresas(){
    this.colecion = this.afs.collection('empresas', ref => ref.orderBy('diamantes', 'desc'));
    return this.colecion.valueChanges();
  }
  
  getEmpresasTop5(){
    this.colecion = this.afs.collection('empresas', ref => ref.orderBy('diamantes', 'desc').limit(5));
    return this.colecion.valueChanges(); 
  }

}
