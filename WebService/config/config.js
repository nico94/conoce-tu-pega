module.exports = {
    nombre: 'API',
    env: process.env.NODE_ENV || 'development',
    puerto: process.env.PORT || 3000,
    base_url: process.env.BASE_URL || 'http://localhost:3000',
    db: {
        uri: process.env.MONGODB_URI || 'mongodb://ctp:Ctp2018@ds133796.mlab.com:33796/conocetupega',
    },
};