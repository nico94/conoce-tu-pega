const http = require('http');
const app = require('./app');
const config = require('./config/config');
const server = http.createServer(app);

server.listen(config.puerto,()=>{
    console.log('Servidor corriendo en puerto: '+config.puerto);
});