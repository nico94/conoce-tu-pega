const express = require('express');
const app = express();
const logger = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require("mongoose");
const config = require('./config/config');

const usuarioRuta = require('./rutas/usuario_ruta');
const empresaRuta = require('./rutas/empresa_ruta');

mongoose.connect(config.db.uri, { useNewUrlParser: true });

app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
      return res.status(200).json({});
    }
    next();
});

//Rutas
app.use("/usuario", usuarioRuta);
app.use("/empresa", empresaRuta);

app.use((req, res, next) => {
  const error = new Error("No encontrado");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;